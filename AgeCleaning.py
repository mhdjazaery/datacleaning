from helper import *

image_path="/media/esb172/OS/DATABASES/imdb-wiki/data/imdb_crop/"
out_path="/media/esb172/4TBSSHD/Datasets/imdb+wiki/imdb_test/"


path="./data/imdb_parent_folder.txt"
sub_folders=readFilesToDictionary(path)

all_keys,dummy=readFiles(path,1)

# read the quality
path="./data/imdb_quality.txt"
Q=readFilesToDictionary(path)

#read the original noisy labels
path="./data/imdb-original-age-label.txt"
original=readFilesToDictionary(path)

#read generated labels
path="./data/imdb_age_prediction_key_corrected.txt"
prediction=readFilesToDictionary(path)

# based on the quality generate the new labels
new_labels={}
Q_avg=56
match=0
f= open('./data/imdb_quality_clean.txt','w')

for key in all_keys:
    original[key]=float(original[key])
    prediction[key]=float(prediction[key])
    Q[key]=float(Q[key])

    diff=abs(original[key] - prediction[key])
    if diff >= 21 and Q[key] > Q_avg:
        new_label = prediction[key]
    elif(diff <= 4) and Q[key] < Q_avg :
        new_label= original[key]
    else:
        fact = 0.7 / (math.exp(float(Q[key]) / Q_avg) )
        fact = 0.7 - fact
        new_label = fact * prediction[key] + (1-fact) * original[key]

        # if Q[key] >= Q_avg :
        #     new_labels[key] = prediction[key]
        #
        # else:
        #     new_labels[key] = original[key]
    new_labels[key]=new_label
    f.write(key + ',' + str(new_label) + '\n')
    # meta = str(original[key]) + "_" + str(prediction[key]) + '_' + str(new_label) + '_' + str(Q[key]) + '_'
    # copyfile(image_path + sub_folders[key] + "/" + key, out_path + "/" + meta + key)

print (match)
