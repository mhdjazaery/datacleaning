__author__ = 'esb172'

import base64
import os
import time
from base64 import decodestring
import numpy as np

file_path = r'/media/esb172/4TBSSHD/downloads/Top1M_MidList.Name.tsv'
img_path = r'/media/esb172/4TBSSHD/Datasets/MS_CELE/test_r/'
rectangle_files = r'/media/esb172/4TBSSHD/Datasets/MS_CELE/align.txt'
wrongimage = r'/media/esb172/4TBSSHD/Datasets/MS_CELE/align_wrong_image_list.txt'
total_count = 0
fold_count = 0
wrong_count =0
with open(file_path,'r+') as f:
    start =time.time()
    for line in f:
        print line
        break
        total_count +=1
        fold_name = line.split()[0]
        # if fold_name=='m.0qfnmpt':
        fold_path = os.path.join(img_path,fold_name)
        if not os.path.exists(fold_path):
            fold_count +=1
            os.makedirs(fold_path)
        imgstring = line.split()[6]
        try:
            imgdata = base64.b64decode(imgstring)
            faceRectangle = base64.b64decode(line.split()[5])
        except Exception:
            wrong_count +=1
            with open(wrongimage,'a') as wi:
                wi.write(fold_name+'\n')
            print('Wrong Image')
            continue
        # tem = decodestring(line.split()[5])
        # print str(tem)
        # print str(faceRectangle)
        if "FaceId" in line.split()[4]:
            filename = str(line.split()[1]) +'-' + line.split()[4] + '.jpg'
            with open(os.path.join(fold_path,filename), 'wb') as f:
                f.write(imgdata)
            with open(rectangle_files, 'ab') as rf:
                # rf.writelines('\n')
                rf.write(filename+' ')
                try :
                    rf.write(str(np.frombuffer(faceRectangle, dtype='float32')))
                except Exception:
                    print ("Wrong float")
                    continue

        print ("finished: {}".format(total_count))
    end =time.time()
    print ("Total time is {}".format(end-start))
    print ("total wrong image:{}".format(wrong_count))
