from helper import *

names_list=[]
count_original_list=[]
count_cleaned_list=[]

path_original="/media/esb172/4TBSSHD/Datasets/MS_CELE/Ms_alignment/"
path_cleaned="/media/esb172/4TBSSHD/Datasets/MS_CELE/MS_DEEP_CLEANING_V_4/"
total_clean_sub_count=0
subdirectories = os.listdir(path_original)
for subdirectory in subdirectories:
    original_folder=os.path.join(path_original , subdirectory)
    clean_folder=os.path.join(path_cleaned , subdirectory)
    if os.path.isdir(clean_folder):
        names_list.append(subdirectory)
        count_original= len(os.listdir(original_folder))
        count_original_list.append(count_original)
        count_clean= len(os.listdir(clean_folder) )
        count_cleaned_list.append(count_clean)
        if count_clean > 0:
            total_clean_sub_count+=1
            print(subdirectory)
            print(count_original)
            print(count_clean)
print(sum(count_cleaned_list))
print(total_clean_sub_count)
out_file=open('evaluation_deep_v4.txt','w')
for i in range(0,len(names_list)):
    line=[]
    line.append(names_list[i])
    line.append(count_original_list[i])
    line.append(count_cleaned_list[i])
    line.append(count_original_list[i]-count_cleaned_list[i])
    line=[str(item) for item in line]
    out_file.write(','.join(line)+"\n")

# import shutil
# final_clean_path='/media/esb172/4TBSSHD/Naved/cfw/super_clean_less_threshold/'
# clean_file=open('noise.txt','w')
# evaluate_file=open('evaluation.txt')
# lines=evaluate_file.readlines()
# for line in lines:
#     line=line.replace('\n','')
#     cols=line.split(',')
#
#     # if         (float(cols[2]) >= 20 and int(cols[2]) < 30 and ( (float(cols[3]) / float(cols[2])) < 0.6 )) \
#     #         or (float(cols[2]) >= 30 and int(cols[2]) < 40 and ( (float(cols[3]) / float(cols[2])) < 0.85 )) \
#     #         or (float(cols[2]) >= 40 and int(cols[2]) < 300 and ( (float(cols[3]) / float(cols[2])) < 0.95 )):
#     if( len(cols)==4 and float(cols[2]) > 5  and ( (float(cols[3]) / float(cols[1])) < 0.5 )):
#
#         clean_file.write(line+'\n')
#         # mkdir(os.path.join(final_clean_path,cols[0]))
#         shutil.copytree(os.path.join(path_cleaned,cols[0]), os.path.join(final_clean_path,cols[0]))
#


# 4864979
# 91796

# 4201324
# 96744


# 4542878
# 96276
