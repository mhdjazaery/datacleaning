import math
import scipy.spatial.distance as dist
import numpy as np
from shutil import copyfile
from shutil import rmtree
import os
from PIL import Image
from sklearn import preprocessing
def  readFiles(path,with_keys=0):
    f=open(path)
    lines=f.readlines()
    keys=[]
    values=[]
    for line in lines:
        line=line.replace("\n","")
        line=line.replace(" ","")
        cols=line.split(",")
        if with_keys:
            keys.append(cols[0])
            values.append(cols[1])
        else:
            values.append(cols)
    f.close()
    if with_keys:
        return keys,values
    else:
        return values


def  readFilesToDictionary(path):
    f=open(path)
    lines=f.readlines()
    dic={}

    for line in lines:
        line=line.replace("\n","")
        line=line.replace(" ","")
        cols=line.split(",")
        dic[cols[0]]=cols[1]
    f.close()
    return dic


def cos_dis(XA,XB,W=None):
    Y = dist.cdist(XA, XB, 'cosine')

    if W==None:
        Y = np.sum(Y,1) / len(XB)
    else:
        Y = Y*W
        Y = np.sum(Y,1) / sum(W)
    return Y


def clean_data(folder_name,main_path,out_path,features_type="cnn"):
    # if features_type=="cnn":
    #CNN
    # simi_limit=0.3
    # simi_limit_final=0.6
    # simi_limit_final_low_quality_factor=0.3
    # qscore_threshold = 58
    # min_accepted_top_quality_number = 1
    # simi_limit_extend=0.45
    qscore_threshold = 54
    simi_limit=0.25
    simi_limit_extend=0.35
    simi_limit_final=0.6
    simi_limit_final_low_quality_factor=0.24

    min_accepted_top_quality_number = 1

    if features_type=="gabor":
        #gabor
        simi_limit=0.47
        simi_limit_final=0.56
        # qscore_threshold = 43
        # min_accepted_top_quality_number = 7
        simi_limit_extend=0.47
        simi_limit_final_low_quality_factor=0.01

    if features_type=="lbp":
        #lbp
        simi_limit=0.79
        simi_limit_final=0.82
        # qscore_threshold = 55
        # min_accepted_top_quality_number = 5
        simi_limit_extend=0.79

    ##############
    # mkdir("/media/guo/NewHDD2TB/MsCeleb1M/originial/"+folder_name,1)

    score_file_path = main_path +folder_name+"/"+ folder_name + "_scores.csv_new"
    features_file_path = main_path + folder_name + "/features/" + folder_name+ "_"+features_type+"fea.npy"


    keys, qscores = readFiles(score_file_path, with_keys=1)
    features = np.load(features_file_path)


    # mean= np.mean(np.array(qscores,dtype=float))
    # get top quality
    top_quality_features = []
    top_quality_keys = []



    for i in range(0,2):
        try:
            indx= i
            top_quality_keys.append(keys[indx])
            top_quality_features.append(features[indx])
            # top_quality_keys.append(keys[indx])
            # top_quality_features.append(features[indx])
            # top_quality_keys.append(keys[indx])
            # top_quality_features.append(features[indx])

        except:
            print "not"

    for i in range(0, len(keys)):
        if float(qscores[i]) > qscore_threshold:
            top_quality_keys.append(keys[i])
            top_quality_features.append(features[i])
    # top_quality_keys=top_quality_keys[:10]
    # top_quality_features=top_quality_features[:10]

    # top_len=len(top_quality_keys)
    # for i in range(0,3):
    #     try:
    #         indx= keys.index(str(i)+"-FaceId-0.jpg")
    #         if qscores[indx] > qscore_threshold:
    #             for j in range(0,int(len(top_quality_keys)/3)):
    #                 top_quality_keys.append(keys[indx])
    #                 top_quality_features.append(features[indx])
    #     except:
    #         print "not"

    # print folder_name
    # print "high quality"
    # print (top_quality_keys)
    # print len(top_quality_keys)

    # get top high correlated
    top_high_correlated_features=[]
    top_high_correlated_keys=[]
    if len(top_quality_keys) > min_accepted_top_quality_number:
        similarity = 1 - cos_dis(top_quality_features, top_quality_features)
        # print similarity
        # sum_sistance_for_each_row=[]
        for i in range(0,len(top_quality_keys)):
            if similarity[i] > simi_limit:
                top_high_correlated_features.append(top_quality_features[i])
                top_high_correlated_keys.append(top_quality_keys[i])
    else:
        print folder_name+" fail"
        return False
    # print "top quality correlated"
    # print (similarity)
    # print (top_high_correlated_keys)
    # print len(top_high_correlated_keys)

    # get top similar to the high correlated
    extend_high_correlated_features=[]
    extend_high_correlated_keys=[]
    if len(top_high_correlated_keys) > min_accepted_top_quality_number:
        similarity = 1 - cos_dis(top_quality_features, top_high_correlated_features)
        # sum_sistance_for_each_row=[]
        for i in range(0,len(top_quality_keys)):
            if similarity[i] > simi_limit_extend:
                extend_high_correlated_features.append(top_quality_features[i])
                extend_high_correlated_keys.append(top_quality_keys[i])
    else:
        print folder_name+" fail"
        return False
    print "after extending "
    print (extend_high_correlated_keys)
    print len(extend_high_correlated_keys)

    # get top similar to the extended high correlated
    final_features=[]
    final_keys=[]

    if len(extend_high_correlated_features) > min_accepted_top_quality_number:
        similarity = 1 - cos_dis(features, extend_high_correlated_features)
        # sum_sistance_for_each_row=[]
        for i in range(0,len(keys)):
            fact=0.45/(1+ math.exp(float(qscores[i])/qscore_threshold-1) * 2)
            # fact=0
            # if float(qscores[i]) < qscore_threshold:
            #     fact=simi_limit_final_low_quality_factor
            #     print keys[i]
            if similarity[i] > (simi_limit_final - fact):
                final_features.append(features[i])
                final_keys.append(keys[i])
                copyfile("/media/esb172/4TBSSHD/Naved/database_aggregation_project/CACD2000/CACD2000_subject_wise_cropped/"+folder_name+"/"+keys[i], out_path+folder_name+"/"+keys[i])
    else:
        print folder_name+" fail"
        return False

    # print (final_keys)
    print "final"
    print (similarity)
    print len(final_keys)
    return True

def mkdir(path,remove_old=0):
    if remove_old and os.path.exists(path):
        rmtree(path)
    if not os.path.exists(path):
        os.makedirs(path)

def dhash(image, hash_size = 8):
    # Grayscale and shrink the image in one step.
    image = image.convert('L').resize(
        (hash_size + 1, hash_size),
        Image.ANTIALIAS,
    )

    # Compare adjacent pixels.
    difference = []
    for row in xrange(hash_size):
        for col in xrange(hash_size):
            pixel_left = image.getpixel((col, row))
            pixel_right = image.getpixel((col + 1, row))
            difference.append(pixel_left > pixel_right)

    # Convert the binary array to a hexadecimal string.
    decimal_value = 0
    hex_string = []
    for index, value in enumerate(difference):
        if value:
            decimal_value += 2**(index % 8)
        if (index % 8) == 7:
            hex_string.append(hex(decimal_value)[2:].rjust(2, '0'))
            decimal_value = 0

    return ''.join(hex_string)

def TheyAreSame(img1,img2):
    orig = Image.open(img1)
    modif = Image.open(img2)
    return dhash(orig) == dhash(modif)

def findDifferencesBetweenFolders(f1,f2):
    res=[]
    subdirectories = os.listdir(f1)
    subdirectories=sorted(subdirectories)
    print subdirectories[:20]
    for subdirectory in subdirectories[:500]:
        original_folder=os.path.join(f1 , subdirectory)
        original_list= [name for name in os.listdir(original_folder) if os.path.isfile(os.path.join(original_folder, name))]
        for img in original_list:
            if not os.path.exists(os.path.join(f2,subdirectory,img)):
                res.append(os.path.join(subdirectory,img))
    return res



def findMatchingBetweenFolders(f1,f2):
    res=[]
    subdirectories = os.listdir(f1)
    subdirectories = sorted(subdirectories)
    print subdirectories[:20]
    for subdirectory in subdirectories[:500]:
        original_folder=os.path.join(f1 , subdirectory)
        original_list= [name for name in os.listdir(original_folder) if os.path.isfile(os.path.join(original_folder, name))]
        for img in original_list:
            if os.path.exists(os.path.join(f2,subdirectory,img)):
                res.append(os.path.join(subdirectory,img))
    return res
#
# 1->200
# 0.632103104862
# 0.756661991585
# 0.688796680498

# 0.57537619699
# 0.834192780643
# 0.681023316062

# 0.566318537859
# 0.788154069767
# 0.659070191431


# 0.578190533253
# 0.713229859571
# 0.638649900728


# 0.578190533253
# 0.78936605317
# 0.667473629604

# 0.582198496568
# 0.767044639012
# 0.661959618481