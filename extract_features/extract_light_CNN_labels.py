import glob
import os
from subprocess import call
from os import listdir
def extract_lightcnn_features(list_path,feature_out_path):

    base_script_path="/media/esb172/4TBSSHD/dev/test_A_and_C_digits"


    jobs_path="/media/esb172/4TBSSHD/dev/digits/digits/jobs/"
    job_id="20170606-175324-0be7"
    job_directory=os.path.join(jobs_path,job_id)


    #extract features

    deploy_file_path=os.path.join(job_directory,"deploy.prototxt")
    mean_values_str="None"

    out_layer="softmax"
    batch_size="75"
    images_dim="140,140"

    #load A_C list
    tmp_list=list_path


    #load A_C list
    new_deploy_path=os.path.join(base_script_path,"out","deploy.prototxt")
    new_deploy_file=open(new_deploy_path,"w")
    deploy_file=open(deploy_file_path,"r")
    newtxt=deploy_file.read().replace('dim: 1','dim:'+ batch_size,1)
    new_deploy_file.write(newtxt)
    new_deploy_file.close()
    deploy_file_path=new_deploy_path


    models=glob.glob(os.path.join(job_directory,'*.caffemodel'))
    models.sort(key=os.path.getmtime)
    for model in models[-1:]:

                extract_features_params=[]
                extract_features_params.append('/usr/bin/python')
                extract_features_params.append(os.path.join(base_script_path,'extract_features_labels.py'))
                extract_features_params.append(tmp_list)
                extract_features_params.append("--batch_size")
                extract_features_params.append(str(int(batch_size)/2))
                extract_features_params.append("--model_def")
                extract_features_params.append(deploy_file_path)
                extract_features_params.append("--mean_file")
                extract_features_params.append(mean_values_str)
                extract_features_params.append("--gpu")
                extract_features_params.append("--images_dim")
                extract_features_params.append(images_dim)
                extract_features_params.append("--out_layer")
                extract_features_params.append(out_layer)
                extract_features_params.append(feature_out_path)
                extract_features_params.append("--pretrained_model")
                extract_features_params.append(model)
                call(extract_features_params)


def rename(s):
    parts=s.split('-')
    parts[0]=parts[0].zfill(8)
    return '-'.join(parts)
def rename_back(s):
    parts=s.split('-')
    parts[0]=str(int(parts[0]))
    return '-'.join(parts)

main_path="/media/esb172/4TBSSHD/Datasets/MS_CELE/Ms_alignment/"
features_type="light_cnn_relabel"
# main_path = "/media/guo/NewHDD2TB/MsCeleb1M/sample_output/"
# out_path = "/media/guo/NewHDD2TB/MsCeleb1M/sample_clean_output/"
subdirectories = os.listdir(main_path)
length=len(subdirectories)
j=0
all=[]
for subdirectoriy in subdirectories:
    j+=1
    if j > 59200 and j < 59900:
        mypath=main_path +subdirectoriy
        onlyfiles = [ main_path+"/"+subdirectoriy+"/"+f for f in listdir(mypath)]
        all.append("\n".join(onlyfiles))
        if j % 400==0:
            features_file_path = "/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel_new/tst_prediction_"+str(j)
            if not os.path.isfile(features_file_path+"fea.npy"):
                path="/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel/tst_all_data_ms_list_"+str(j)+".txt"
                # file=open(path,"w")
                # file.write("\n".join(all) +"")
                #file.close()

                list_file_path = path
                extract_lightcnn_features(list_file_path,features_file_path)
            all=[]

path="/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel/tst_all_data_ms_list_"+str(j)+".txt"
# file=open(path,"w")
# file.write("\n".join(all) +"")
# file.close()
features_file_path = "/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel_new/tst_prediction_"+str(j)
list_file_path = path
extract_lightcnn_features(list_file_path,features_file_path)
all=[]
# print(np.load("/media/esb172/4TBSSHD/dev/data_cleaning/predictionfea.npy"))