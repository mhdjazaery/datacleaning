import glob
import os
import re
from subprocess import call
import numpy as np

def extract_lightcnn_features(list_path,feature_out_path):

    base_script_path="/media/esb172/4TBSSHD/dev/test_A_and_C_digits"


    jobs_path="/media/esb172/4TBSSHD/dev/digits/digits/jobs/"
    job_id="20170221-162142-7ed4"
    job_directory=os.path.join(jobs_path,job_id)


    #extract features

    deploy_file_path=os.path.join(job_directory,"deploy.prototxt")
    mean_values_str="None"

    out_layer="eltwise_fc1"
    batch_size="5"
    images_dim="140,140"

    #load A_C list
    tmp_list=list_path


    #load A_C list
    new_deploy_path=os.path.join(base_script_path,"out","deploy.prototxt")
    new_deploy_file=open(new_deploy_path,"w")
    deploy_file=open(deploy_file_path,"r")
    newtxt=deploy_file.read().replace('dim: 1','dim:'+ batch_size,1)
    new_deploy_file.write(newtxt)
    new_deploy_file.close()
    deploy_file_path=new_deploy_path


    models=glob.glob(os.path.join(job_directory,'*.caffemodel'))
    models.sort(key=os.path.getmtime)
    for model in models[-1:]:

                extract_features_params=[]
                extract_features_params.append('/usr/bin/python')
                extract_features_params.append(os.path.join(base_script_path,'extract_features.py'))
                extract_features_params.append(tmp_list)
                extract_features_params.append("--batch_size")
                extract_features_params.append(str(int(batch_size)/2))
                extract_features_params.append("--model_def")
                extract_features_params.append(deploy_file_path)
                extract_features_params.append("--mean_file")
                extract_features_params.append(mean_values_str)
                extract_features_params.append("--gpu")
                extract_features_params.append("--images_dim")
                extract_features_params.append(images_dim)
                extract_features_params.append("--out_layer")
                extract_features_params.append(out_layer)
                extract_features_params.append(feature_out_path)
                extract_features_params.append("--pretrained_model")
                extract_features_params.append(model)
                print(extract_features_params)
                call(extract_features_params)





