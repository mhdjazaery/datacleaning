from collections import Counter
import re

__author__ = 'esb172'
import numpy as np

from_folder = "/media/esb172/4TBSSHD/Datasets/MS_CELE/MsCele_lightened_lightened_out/"
to_folder = "/media/esb172/4TBSSHD/Datasets/MS_CELE/MS_MANUALLY_CHINESE/"

eth = set()
eth_file = open('/home/esb172/dev/data_cleaning/data/Manually_MS_Chinese.txt')
lines = eth_file.readlines()
for line in lines:
    line = line.replace("\n", "")
    line = line.replace("\r", "")

    line = line.split(',')
    if len(line)==2:
        eth.add(line[1])


file_out = open('../clean_subject_bootstrap.txt','w')
count=0
file = open('/home/esb172/dev/data_cleaning/data/MS-Celeb-1M_clean_list.txt')
lines = file.readlines()
ids_folders_dic = {}
ids = set()
for line in lines:
    line = line.replace("\n", "")
    folder = line.split()[0].split("/")[0]
    id = line.split()[1]
    if ids_folders_dic.get(id, "none") == "none":
        ids_folders_dic[id]=[]
    ids_folders_dic[id].append(folder)
    ids.add(id)


for id in ids:
    A = np.array(ids_folders_dic[id])
    b=Counter(A.flat).most_common(1)
    print(b[0])
    file_out.write(b[0][0]+"\n")



    # if ids_folders_dic[id] in eth:
    #     if not os.path.isdir(os.path.join(to_folder, id)) and os.path.isdir(os.path.join(from_folder, id)):
    #         copytree(os.path.join(from_folder, id), os.path.join(to_folder, id))
    #         count+=1
    #         print(count)
