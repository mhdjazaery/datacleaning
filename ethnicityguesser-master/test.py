__author__ = 'esb172'
import re
import os
from shutil import copytree
import python_http_client
import json

#
# host = "http://www.textmap.com"
#
# client = python_http_client.Client(host=host)
#
# data = {
#    "names" : [ "George Washington", "John Smith", "Barack Obama" ]
# }
#
# response = client.ethnicity_api.api.post(request_body=data)
# json_response = json.loads(response.body)
# print(json_response['Barack Obama'][0]['best'])
# exit()
import cPickle as pickle
pickle_file = open('/media/esb172/4TBSSHD/downloads/ethnicityguesser-master/pickled_classifiers/danish_irish_chinese_czech_japanese_french_jewish_indian_spanish_italian_.pkl', 'rb')

mxec = pickle.load(pickle_file)
pickle_file.close()

file_path = r'/home/esb172/dev/data_cleaning/data/Top1M_MidList.Name.tsv'
from_folder="/media/esb172/4TBSSHD/Datasets/MS_CELE/Ms_alignment/"
to_folder="/media/esb172/4TBSSHD/Datasets/MS_CELE/MS_Asian/"
total_count = 0
fold_count = 0
wrong_count =0
eth_list=['chinese','japanese','indian']
language_code_list=['en']
chinese_ethnicity_file=open("/home/esb172/dev/data_cleaning/data/ASIAN_subject_ids.txt",'w')
with open(file_path,'r+') as f:
    for line in f:
        cols=line.split()
        folder=cols[0]
        m = re.search("\@([\w]+)",str(line))

        language_code=m.groups(0)[0]
        if language_code in language_code_list:
            m = re.search(" ([^ ]+)\"",str(line))

            if hasattr(m, 'groups'):
                last_name=m.groups(0)[0]
                # print(last_name)
                # print(mxec.classify(last_name))
                eth=mxec.classify(last_name)
                if eth in eth_list:
                    # print(eth)
                    # if not os.path.isdir(os.path.join(to_folder,folder)) and os.path.isdir(os.path.join(from_folder,folder)):
                    #     copytree(os.path.join(from_folder,folder), os.path.join(to_folder,folder),)
                    total_count+=1
                    chinese_ethnicity_file.write(folder+"\n")

print(total_count)

