import os
import shutil
__author__ = 'esb172'
from_dir=""
to_dir="/media/esb172/4TBSSHD/Datasets/MS_CELE/clean_plus_labels_correction_over_0.9/"


path_original="/media/esb172/4TBSSHD/Datasets/MS_CELE/MS_DEEP_CLEANING_V_3/"

subdirectories = os.listdir(path_original)
for subdirectory in subdirectories:
    directory=os.path.join(to_dir , subdirectory)
    if not os.path.exists(directory):
            os.makedirs(directory)
    original_folder=os.path.join(path_original , subdirectory)
    images= [name for name in os.listdir(original_folder) if os.path.isfile(os.path.join(original_folder, name))]
    for image in images:
        out_image_path=os.path.join(to_dir,subdirectory,image+'_'+subdirectory+'.jpg')
        if not os.path.exists(out_image_path):
            shutil.copyfile(os.path.join(original_folder,image),out_image_path)

# lable_file=open("/media/esb172/4TBSSHD/dev/digits/digits/jobs/20170331-172745-3e90/val.txt")
# lines=lable_file.readlines()
#
# labels_dic={}
# for line in lines:
#     line=line.replace("\n","")
#     cols=line.split(" ")
#     labels_dic[cols[1]]=cols[0].split("/")[-2]
#
# labels_ls=[]
# names_ls=[]
# probability=[]
# paths_ls=[]
#
# def read_data(i):
#     global labels_ls,probability,paths_ls,names_ls
#     lable_file=open("/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel_new/tst_prediction_"+str(i)+"network_predict_label_class.csv")
#     lines=lable_file.readlines()
#
#     for line in lines:
#         line=line.replace("\n","")
#         cols=line.split(" ")
#         names_ls.append(cols[0])
#         labels_ls.append(cols[1])
#         probability.append(cols[2])
#
#     lable_file=open("/media/esb172/4TBSSHD/dev/data_cleaning/prediction_relabel/tst_all_data_ms_list_"+str(i)+".txt")
#     lines=lable_file.readlines()
#
#     for line in lines:
#         line=line.replace("\n","")
#         cols=line.split(" ")
#         paths_ls.append(cols[0])
#
#
# for i in range(400,99891,400):
#     print(i)
#     read_data(i)
#
# read_data(99891)
#
#
#
#
# # print(len(probability))
# # print(len(labels_ls))
# # print(len(paths_ls))
# # exit()
# Threshold=0.9
# count=0
# for i in range(0,len(paths_ls)):
#     # print(i)
#     if float(probability[i]) > Threshold :
#         directory=path_original+"/"+str(labels_dic[labels_ls[i]])
#         directory_to=to_dir+"/"+str(labels_dic[labels_ls[i]])
#         if not os.path.exists(directory_to):
#             os.makedirs(directory_to)
#         if not os.path.exists(directory+"/"+str(names_ls[i])):
#             # shutil.copyfile(paths_ls[i],directory_to+"/"+str(names_ls[i])+"_"+str(paths_ls[i].split('/')[-2])+".jpg")
#             count+=1
#             print count
# print Threshold
# print count

# 0.6
# 2597623
# 0.5
# 2976280
# 0.4
# 3328761
# 0.3
# 3670646
# 0.2
# 4038664
# 0.1
# 4546613
# 0.09
# 4622728