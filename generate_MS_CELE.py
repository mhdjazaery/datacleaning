__author__ = 'esb172'
import re
import os
from shutil import copytree
import python_http_client
import json


host = "http://www.textmap.com"

client = python_http_client.Client(host=host)

data = {
   "names" : [ "George Washington", "John Smith", "Barack Obama" ]
}

response = client.ethnicity_api.api.post(request_body=data)
json_response = json.loads(response.body)
print(json_response['Barack Obama'][0]['best'])
exit()
file_path = r'/home/esb172/dev/data_cleaning/data/Top1M_MidList.Name.tsv'
from_folder="/media/esb172/4TBSSHD/Datasets/MS_CELE/Ms_alignment/"
to_folder="/media/esb172/4TBSSHD/Datasets/MS_CELE/MS_ASIAN/"
total_count = 0
fold_count = 0
wrong_count =0
asian_language_code_list=['zh']
with open(file_path,'r+') as f:
    for line in f:
        cols=line.split()
        folder=cols[0]
        m = re.search("\@([\w]+)",str(line))

        language_code=m.groups(0)[0]
        if language_code in asian_language_code_list:
            print(folder)
            if not os.path.isdir(os.path.join(to_folder,folder)) and os.path.isdir(os.path.join(from_folder,folder)):
                copytree(os.path.join(from_folder,folder), os.path.join(to_folder,folder),)
                print(line)
                total_count+=1
print(total_count)