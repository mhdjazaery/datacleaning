from helper import *


# main_path = "/media/guo/Hard1/Downloads/output/"
main_path = "/media/esb172/4TBSSHD/Naved/database_aggregation_project/CACD2000/CACD2000_subject_wise_rqs_data/"
#main_path = "/media/esb172/4TBSSHD/Datasets/MS_CELE/test_tmp_1/"
out_path = "/media/esb172/4TBSSHD/Naved/database_aggregation_project/CACD2000/CACD2000_subject_quality_deep_cleaning/"
#out_path = "/media/esb172/4TBSSHD/Datasets/MS_CELE/test_res/"

# main_path = "/media/guo/NewHDD2TB/MsCeleb1M/sample_output/"
# out_path = "/media/guo/NewHDD2TB/MsCeleb1M/sample_clean_output/"
subdirectories = os.listdir(main_path)
for subdirectoriy in subdirectories:
    folder_name=subdirectoriy
    # copytree("/media/guo/NewHDD2TB/Ms/MsCele_align/"+folder_name, "/media/guo/NewHDD2TB/MsCeleb1M/originial/"+folder_name)
    mkdir(out_path+folder_name,remove_old=1)
    clean_data(folder_name,main_path,out_path,features_type="light_cnn")
    # clean_data(folder_name,main_path,out_path,features_type="lbp")
    # clean_data(folder_name,main_path,out_path,features_type="gabor")


