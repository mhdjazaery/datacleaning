from helper import *

names_list=[]
count_original_list=[]
count_cleaned_list=[]

# path_original="/media/guo/Hard2/dataset/CAS-PEAL-R1/aligned/"
# out_path="/media/guo/Hard2/dataset/CAS-PEAL-R1/subject-wise_aligned/"

# path_original="/media/guo/4TBSSHD/Datasets/PaSC/pre_processing/video/training/aligned/"
# out_path="/media/guo/4TBSSHD/Datasets/PaSC/subject_wise/"

path_original="/media/esb172/4TBSSHD/Datasets/a_and_c/aligned/original/"
out_path="/media/esb172/4TBSSHD/Datasets/a_and_c/aligned/subject_wise/"

# db_prefix='CAS-PEAL-R1'
db_prefix='A_and_C'
subdirectories = os.listdir(path_original)
for subdirectory in subdirectories:
    original_folder=os.path.join(path_original , subdirectory)
    images= [name for name in os.listdir(original_folder) if os.path.isfile(os.path.join(original_folder, name))]
    for image in images:
        # subject_id= image.split('_')[1]
        # subject_id= image.split('_')[0]
        # subject_id= image.split('d')[0]
        subject_id= image.split('_')[0]
        mkdir(os.path.join(out_path,db_prefix+'_'+subject_id))
        out_image_path=os.path.join(out_path,db_prefix+'_'+subject_id,db_prefix+'_'+subject_id+'_'+image)
        copyfile(os.path.join(original_folder,image),out_image_path)

